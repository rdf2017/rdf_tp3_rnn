/**
 * 
 */
package rdf_tp3_rnn;

/**
 * Généralisation des neurones perceptrons utilisés dans le module RDF
 * 
 * @author maxim
 */
public abstract class ANeuron implements INeuron {
	private final Double[] weights;
	private ActivationFunction activationFunction;
	private Double learnRate = 0.3d;

	protected ANeuron(Double[] weights) {
		this.weights = weights;
	}
	
	public final void setActivationFunction(ActivationFunction activationFunction) {
		this.activationFunction = activationFunction;
	}
	
	public final void setLearnRate(Double learnRate) {
		this.learnRate = learnRate;
	}
	
	protected Double computeWeightedInput(Double[] sample) {
		Double res = weights[0];
		for (int i = 0; i < sample.length; i++) {
			res += sample[i] * weights[i+1];
		}
		return res;
	}
	
	@Override
	public Double sigma(Double[] sample) {
		final Double u = computeWeightedInput(sample);
		return getActivationFunction()
				.getFunction()
				.run(u);
	}
	
	@Override
	public Double sigmap(Double[] sample) {
		return getActivationFunction()
				.getDerived()
				.run(sigma(sample));
	}
	
	@Override
	public Double[] getWeights() {
		return weights;
	}

	/**
	 * @return the activationFunction
	 */
	protected final ActivationFunction getActivationFunction() {
		return activationFunction;
	}

	/**
	 * @return the learnRate
	 */
	protected final Double getLearnRate() {
		return learnRate;
	}

	@Override
	public Double[] applyWeightsDelta(Double[] delta) {
		for (int i = 0; i < delta.length; i++) {
			getWeights()[i] += delta[i];
		}
		return getWeights();
	}
}
