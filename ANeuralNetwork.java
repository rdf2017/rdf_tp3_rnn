package rdf_tp3_rnn;

public abstract class ANeuralNetwork implements INeuralNetwork {
	protected INeuron[][] layers;
	
	public ANeuralNetwork() {}
	
	/**
	 * @param layers
	 */
	public ANeuralNetwork(INeuron[][] layers) {
		setNetwork(layers);
	}
	
	/**
	 * Throws an IllegalStateException if the neural network is invalid
	 */
	private void checkLayers() {
		if (layers != null && layers.length > 0) {
			for (INeuron[] layer : layers) {
				if (layer != null && layer.length > 0) {
					for (INeuron neuron : layer) {
						if (neuron == null) {
							throw new IllegalStateException("Neural network neurons must not be null");
						}
					}
				} else {
					throw new IllegalStateException("Neural network layers must have at least 1 neuron");
				}
			}
		} else {
			throw new IllegalStateException("Neural network must have at least 1 layer");
		}
	}
		
	@Override
	public Double[][][] getAllLayerWeights() {
		final Double[][][] res = new Double[layers.length][][];
		for (int lIdx = 0; lIdx < layers.length; lIdx++) {
			res[lIdx] = new Double[layers[lIdx].length][];
			for (int nIdx = 0; nIdx < layers[lIdx].length; nIdx++) {
				res[lIdx][nIdx] = layers[lIdx][nIdx].getWeights();
			}
		}
		return res;
	}
	
	public void setNetwork(INeuron[][] network) {
		this.layers = network;
		checkLayers();
	}
}
