package rdf_tp3_rnn;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

/**
 * 
 * @author maxim
 *
 */
public class Main extends Application {
	private int maxIters = 100;
	private ActivationFunction activationFunction;
	private final static Random RAND = new Random(System.currentTimeMillis());
	private Class<? extends ANeuron> neuronClass;
	private final ANeuralNetwork network = new NeuralNetwork();
	private boolean displayIter, displayFirst, updateClasses, displayWeightsLine, randomWeightsTraining;
	private List<Double[]> learningSamples;
	private List<Double> desiredClasses, learningClasses;
	
	public static void main(String[] args) throws FileNotFoundException {
		launch(args);
	}
	
	private int train(boolean displayEachIter) {
		int iter = 0;
		int nbErrors;
		double cumulatedError;
		final boolean isWidrowHoff = neuronClass == WidrowHoffNeuron.class; 
		do {
			cumulatedError = 0d;
			nbErrors = 0;
			for (int i = 0; i < learningSamples.size(); i++) {
				final Double tmperr = network.train(learningSamples.get(i), desiredClasses.get(i));
				if (updateClasses) {
					final Double obtainedClass = desiredClasses.get(i) - tmperr;
					final Double distToMin = Math.abs(obtainedClass - activationFunction.getMinValue());
					final Double distToMax = Math.abs(obtainedClass - activationFunction.getMaxValue());
					if (distToMin > distToMax) {
						learningClasses.set(i, activationFunction.getMaxValue());
					} else {
						learningClasses.set(i, activationFunction.getMinValue());
					}
				}
				if (Math.abs(tmperr) > activationFunction.getErrorTreshold()) {
					nbErrors++;	// si widrowhoff : iter jusqu'� 100; sinon uniquement si erreur > seuil
				}
				cumulatedError += tmperr;
			}
			iter++;
			if (displayEachIter && iter < maxIters && (iter < 5 || (iter < 101 && iter % 10 == 0) || (iter < 1001 && iter % 100 == 0) || iter % 1000 == 0)) {
				System.out.println("Iteration n�" + iter + " : " + nbErrors + " classification errors, accumulating to " + cumulatedError + " for the network");
				if (updateClasses) {
					displayChart(new Stage(), spreadSeriesByClasses(learningSamples, learningClasses), "Iteration n�" + iter);
				} else {
					displayChart(new Stage(), spreadSeriesByClasses(learningSamples, desiredClasses), "Iteration n�" + iter);
				}
			}
		} while (iter < maxIters && (nbErrors > 0 || isWidrowHoff));
		return nbErrors;
	}
	
	private double decide(Double[] sample) {
		return network.decide(sample);
	}
	
	private static List<Double[]> getSamplesFromFile(String filePath) throws FileNotFoundException {
		final List<List<Double>> dimsSamples = new ArrayList<>();
		final File input = new File(filePath);
		final Scanner scan = new Scanner(input);
		String line;
		while (scan.hasNextLine() && !(line = scan.nextLine()).equals("")) {
			final List<Double> dimSamples = new ArrayList<>();
			final Scanner lineScan = new Scanner(line);
			lineScan.useDelimiter(" ");
			while (lineScan.hasNext()) {
				final String toParse = lineScan.next();
				try {
					dimSamples.add(Double.parseDouble(toParse));
				} catch (NumberFormatException e) {}
			}
			dimsSamples.add(dimSamples);
			lineScan.close();
		}
		scan.close();
		
		final List<Double[]> samples = new ArrayList<Double[]>();
		for (int sIdx = 0; sIdx < dimsSamples.get(0).size(); sIdx++) {
			final Double[] sample = new Double[dimsSamples.size()];
			for (int dIdx = 0; dIdx < dimsSamples.size(); dIdx++) {
				sample[dIdx] = dimsSamples.get(dIdx).get(sIdx);
			}
			samples.add(sample);
		}
		return samples;
	}
	
	private List<XYChart.Series<Number, Number>> computeWeightsLine() {
		final List<XYChart.Series<Number, Number>> res = new ArrayList<>(2);
		res.add(new XYChart.Series<>());
		res.add(new XYChart.Series<>());
		
		final Double[] min = new Double[]{Double.MAX_VALUE, Double.MAX_VALUE};
		final Double[] max = new Double[]{Double.MIN_VALUE, Double.MIN_VALUE};
		for (Double[] coords : learningSamples)   {
			if (coords[0] <= min[0]) min[0] = coords[0];
			if (coords[0] >= max[0]) max[0] = coords[0];
			if (coords[1] <= min[1]) min[1] = coords[1];
			if (coords[1] >= max[1]) max[1] = coords[1];
		}
		
		final Double[][][] allNeuronWeights = network.getAllLayerWeights();
		for (int nIdx = 0; nIdx < allNeuronWeights[0].length; nIdx++) {	// pour chaque neurone de la couche 0
			final Double[] weights = allNeuronWeights[0][nIdx];
			final Double[] weightsPoint1 = new Double[]{min[0], (weights[0] + weights[1] * min[0]) / -weights[2]}; 
			final Double[] weightsPoint2 = new Double[]{max[0], (weights[0] + weights[1] * max[0]) / -weights[2]};
			res.get(nIdx).getData().add(new XYChart.Data<Number, Number>(weightsPoint1[0], weightsPoint1[1]));
			res.get(nIdx).getData().add(new XYChart.Data<Number, Number>(weightsPoint2[0], weightsPoint2[1]));
		}
		
		return res;
	}
	
	private static Collection<Collection<Double[]>> spreadSeriesByClasses(List<Double[]> samples, List<Double> classes) {
		final Map<Double, Collection<Double[]>> series = new HashMap<>();
		for (int i = 0; i < samples.size(); i++) {
			final Double classValue = classes.get(i);
			Collection<Double[]> serie = series.get(classValue);
			if (serie == null) {
				serie = series.put(classValue, new ArrayList<>());
			}
			serie = series.get(classValue);
			serie.add(samples.get(i));
		}
		return series.values();
	}
	
	/**
	 * Assumes 2D samples
	 * @param stage
	 * @param series
	 * @param titre
	 */
	private void displayChart(Stage stage, Collection<Collection<Double[]>> series, String titre) {
		stage.setTitle(titre);
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();        
        final LineChart<Number,Number> sc = new LineChart<>(xAxis,yAxis);
        xAxis.setLabel("X");                
        yAxis.setLabel("Y");
        sc.setTitle(titre);
        sc.setLegendVisible(false);

        final List<XYChart.Series<Number, Number>> seriesJFX = new ArrayList<>(series.size());
        if (displayWeightsLine) {
        	seriesJFX.addAll(computeWeightsLine());
        }
        else {
        	seriesJFX.add(new XYChart.Series<>());
        	seriesJFX.add(new XYChart.Series<>());
        }
        
        for (Collection<Double[]> serie : series) {
        	final XYChart.Series<Number, Number> serieJFX = new XYChart.Series<>();
        	seriesJFX.add(serieJFX);
        	for (Double[] coords : serie) {
            		serieJFX.getData().add(new XYChart.Data<>(coords[0], coords[1]));
        	}
        }

        sc.getData().addAll(seriesJFX);
        Scene scene  = new Scene(sc, 500, 400);
        scene.getStylesheets().add(getClass().getResource("chart.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

	private static List<Double[]> get4Samples() {
		final List<Double[]> ORSamples = new ArrayList<>(4);
		ORSamples.add(new Double[]{-1d, -1d});
		ORSamples.add(new Double[]{-1d, 1d});
		ORSamples.add(new Double[]{1d, -1d});
		ORSamples.add(new Double[]{1d, 1d});
		return ORSamples;
	}
	
	private static List<Double> getORClasses(ActivationFunction activationFunction) {
		final List<Double> ORClasses = new ArrayList<>(4);
		ORClasses.add(activationFunction.getMinValue());
		ORClasses.add(activationFunction.getMaxValue());
		ORClasses.add(activationFunction.getMaxValue());
		ORClasses.add(activationFunction.getMaxValue());
		return ORClasses;
	}
	
	private static List<Double> getXORClasses(ActivationFunction activationFunction) {
		final List<Double> ORClasses = new ArrayList<>(4);
		ORClasses.add(activationFunction.getMinValue());
		ORClasses.add(activationFunction.getMaxValue());
		ORClasses.add(activationFunction.getMaxValue());
		ORClasses.add(activationFunction.getMinValue());
		return ORClasses;
	}
	
	private static Double[] getRandomWeights(double min, double max, int amount) {
		final Double[] weights = new Double[amount];
		for (int i = 0; i < amount; i++) {
			weights[i] = min + (max - min) * RAND.nextDouble();
		}
		return weights;
	}
	
	private static Double[][][] getTestWeights() {
		final Double[][][] weights = new Double[2][][];
		weights[0] = new Double[2][];
		weights[1] = new Double[1][];
		weights[0][0] = new Double[]{-.5d,2d,-1d};
		weights[0][1] = new Double[]{.5d,.5d,1d};
		weights[1][0] = new Double[]{2d,-1d,1d};
		return weights;
	}
	
	private void initCommandLineArguments(Map<String, String> args) throws FileNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {

		// set activationFunction
		activationFunction = ActivationFunction.get(Integer.parseInt(args.getOrDefault("active", "1")));

		switch(args.getOrDefault("randomw", "true").toLowerCase()) {
		case "false" :
		case "f" :
		case "0" :
		case "no" :
		case "n" :
		case "wrong" :
		case "w" :
			randomWeightsTraining = false;
			break;
		case "true" :
		case "t" :
		case "yes" :
		case "y" :
		case "1":
			randomWeightsTraining = true;
			break;
		default :
			throw new IllegalArgumentException("Argument " + args.get("randomw") + " for the 'randomw' parameter is unknown");
		}
		
		if (randomWeightsTraining) {
			// learning samples with input file or OR or XOR default samples and classes
			final String input = args.getOrDefault("input", "or");
			switch(input) {
			case "or" : 
				learningSamples = get4Samples();
				desiredClasses = getORClasses(activationFunction);
				break;
			case "xor" :
				learningSamples = get4Samples();
				desiredClasses = getXORClasses(activationFunction);
				break;
			default :
				learningSamples = getSamplesFromFile(input);
				desiredClasses = new ArrayList<>(learningSamples.size());
				for (int i = 0; i < learningSamples.size(); i++) {
					if (i < learningSamples.size() / 2) desiredClasses.add(activationFunction.getMinValue());
					else desiredClasses.add(activationFunction.getMaxValue());
				}
			}
		}
		
		// training algorithm
		switch (args.getOrDefault("algo", "w").toLowerCase()) {
		case "simple" :
		case "s" :
			neuronClass = SimpleNeuron.class;
			break;
		case "widrowhoff" :
		case "widrow" :
		case "hoff" :
		case "wh" :
		case "w" :
			neuronClass = WidrowHoffNeuron.class;
			break;
//		case "none" :
//			neuronClass = null;
		default :
			throw new IllegalArgumentException("Argument " + args.get("algo") + " for the 'algo' parameter is unknown");
		}
		
		// display type
		switch(args.getOrDefault("display", "end").toLowerCase()) {
		case "end" :
		case "e" :
			displayFirst = false;
			displayIter = false;
			break;
		case "compare" :
		case "comp" :
		case "c" :
			displayFirst = true;
			displayIter = false;
			break;
		case "iter" :
		case "iteration" :
		case "each" :
		case "all" :
		case "i" :
			displayFirst = true;
			displayIter = true;
			break;
		default :
			throw new IllegalArgumentException("Argument " + args.get("display") + " for the 'display' parameter is unknown");
		}

		// display updated classes or old
		switch (args.getOrDefault("updateclasses", "false").toLowerCase()) {
		case "false" :
		case "f" :
		case "0" :
		case "no" :
		case "n" :
		case "wrong" :
		case "w" :
			updateClasses = false;
			break;
		case "true" :
		case "t" :
		case "yes" :
		case "y" :
		case "1":
			updateClasses = true;
			break;
		default :
			throw new IllegalArgumentException("Argument " + args.get("display") + " for the 'display' parameter is unknown");
		}
		
		// display weights line
		switch(args.getOrDefault("weightsline", "true").toLowerCase()) {
		case "false" :
		case "f" :
		case "0" :
		case "no" :
		case "n" :
		case "wrong" :
		case "w" :
			displayWeightsLine = false;
			break;
		case "true" :
		case "t" :
		case "yes" :
		case "y" :
		case "1":
			displayWeightsLine = true;
			break;
		default :
			throw new IllegalArgumentException("Argument " + args.get("display") + " for the 'display' parameter is unknown");
		}
		
		// max iterations
		maxIters = Integer.parseInt(args.getOrDefault("maxiter", "100"));
		
		// Create the neural network
		ANeuron[][] layers;
		switch(args.getOrDefault("network", "single").toLowerCase()) {
		case "single" :
		case "s" :
		case "solo" :
			layers = new ANeuron[1][];
			layers[0] = new ANeuron[1];
			break;
		case "multi" :
		case "m" :
			layers = new ANeuron[2][];
			layers[0] = new ANeuron[2];
			layers[1] = new ANeuron[1];
			break;
		default :
			throw new IllegalArgumentException("Argument " + args.get("network") + " for the 'network' parameter is unknown");
		}
		
		if (!randomWeightsTraining && args.getOrDefault("network", "single").equals("single")) {
			throw new IllegalStateException("Fixed weights is only for multi layer neural network (ex3.1");
		}
		Double[][][] testWeights = null;
		if (!randomWeightsTraining) {
			testWeights = getTestWeights();
		}
		
		for (int lIdx = 0; lIdx < layers.length; lIdx++) {
			for (int nIdx = 0; nIdx < layers[lIdx].length; nIdx++) {
				layers[lIdx][nIdx] = neuronClass.getDeclaredConstructor(Double[].class)
						.newInstance(new Object[]{randomWeightsTraining ? getRandomWeights(-1d, 1d, 3) : testWeights[lIdx][nIdx]});
				layers[lIdx][nIdx].setActivationFunction(activationFunction);
			}
		}
		network.setNetwork(layers);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
//		for (int i = 0; i < 100; i++) {
		initCommandLineArguments(getParameters().getNamed());
		
		if (randomWeightsTraining) {
			learningClasses = new ArrayList<>(desiredClasses.size());
			for(Double classification : desiredClasses) {
				learningClasses.add(new Double(classification));
			}
			
			if (displayFirst) {
				displayChart(new Stage(), spreadSeriesByClasses(learningSamples, desiredClasses), displayWeightsLine?"before":"desired");
			}
			
			// Train the neural network
			final int nbErrors = train(displayIter);
			System.out.println("Il subsiste " + nbErrors + " diff�rences entre les classifications d�sir�es et obtenues");
			
			// Display result
			if (updateClasses) {
				displayChart(new Stage(), spreadSeriesByClasses(learningSamples, learningClasses), "result");
			} else {
				displayChart(new Stage(), spreadSeriesByClasses(learningSamples, desiredClasses), "result");
			}
		} else {
			final Double[] sampleToDecide = new Double[]{1d,1d};
			final double res = decide(sampleToDecide);
			System.out.println(res);
		}
//		}
	}
}
