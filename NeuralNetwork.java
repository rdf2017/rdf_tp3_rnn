package rdf_tp3_rnn;

/**
 * 
 * @author maxim
 *
 * @param <INeuron> Neuron type
 */
public class NeuralNetwork extends ANeuralNetwork {
	
	public NeuralNetwork() {super();}
	
	/**
	 * @param layers
	 */
	public NeuralNetwork(INeuron[][] layers) {
		super(layers);
	}
	
	private Double[][] decideAllNeurons(Double[] sample) {
		// Compute result for each neuron (from input to output)
		final Double[][] results = new Double[layers.length][];	// cr�e tableau des r�sultats des couches
		results[0] = new Double[layers[0].length];
		for (int nIdx = 0; nIdx < layers[0].length; nIdx++) {	// pour la couche d'entr�e
			results[0][nIdx] = layers[0][nIdx].decide(sample);	// applique d�cision au neurone et renseigne r�sultat
		}
		for (int lIdx = 1; lIdx < layers.length; lIdx++) {	// pour chaque couche suivante
			results[lIdx] = new Double[layers[lIdx].length];	// cr�e tableau des r�sultats des neurones par couche
			for (int nIdx = 0; nIdx < layers[lIdx].length; nIdx++) {	// pour chaque neurone
				results[lIdx][nIdx] = layers[lIdx][nIdx].decide(results[lIdx-1]);	// applique d�cision au neurone et renseigne r�sultat
			}
		}
		return results;
	}
	
	@Override
	public Double decide(Double[] sample) {
		return decideAllNeurons(sample)[layers.length-1][0];
	}
	
	@Override
	public Double train(Double[] sample, Double desired) {
		// Compute result for each neuron (from input to output)
		final Double[][] results = decideAllNeurons(sample);
		// Compute error for each neuron (from output to input)
		final Double[][] errors = new Double[layers.length][];
		{
			// For output layer
			errors[layers.length-1] = new Double[layers[layers.length-1].length];
			for (int nIdx = 0; nIdx < layers[layers.length-1].length; nIdx++) {
				errors[layers.length-1][nIdx] = (desired - results[layers.length-1][nIdx]) ;//* layers[layers.length-1][nIdx].sigmap(results[layers.length-1]);	// erri = (yi - di) * sigmapi
			}
			
			// For hidden layers
			for (int lIdx = layers.length-2; lIdx >= 0; lIdx--) {	// pour chaque couche cach�e
				errors[lIdx] = new Double[layers[lIdx].length];	// cr�e tableau des erreurs des neurones par couche
				for (int nIdx = 0; nIdx < layers[lIdx].length; nIdx++) {	// pour chaque neurone
					errors[lIdx][nIdx] = 0d;
					// Compute sum from next layer neuron's weights and errors
					for (int pnIdx = 0; pnIdx < layers[lIdx+1].length; pnIdx++) {	// pour chaque neurone de la couche suivante
						errors[lIdx][nIdx] += errors[lIdx+1][pnIdx] * layers[lIdx+1][pnIdx].getWeights()[nIdx+1];	// err[c][i] = SUM[j] (err[c+1][j] * w[c+1][j][i+1 (seuil)] 
					}
					try {
						errors[lIdx][nIdx] *= layers[lIdx][nIdx].sigmap(lIdx == 0 ? sample : results[lIdx-1]);	// err[c][i] *= sigmap()
					} catch (UnsupportedOperationException e) {
						e.printStackTrace();
					}
				}
			}
		}
		// Compute weights correction
		final Double[][][] weightsDeltas = new Double[layers.length][][];
		for (int lIdx = 0; lIdx < layers.length; lIdx++) {
			weightsDeltas[lIdx] = new Double[layers[lIdx].length][];
			for (int nIdx = 0; nIdx < layers[lIdx].length; nIdx++) {
				weightsDeltas[lIdx][nIdx] = layers[lIdx][nIdx].computeWeightsDelta(lIdx == 0 ? sample : results[lIdx-1], errors[lIdx][nIdx], desired);
			}
		}
		// Apply weights correction
		for (int lIdx = 0; lIdx < layers.length; lIdx++) {
			for (int nIdx = 0; nIdx < layers[lIdx].length; nIdx++) {
				layers[lIdx][nIdx].applyWeightsDelta(weightsDeltas[lIdx][nIdx]);
			}
		}
		// Return sum of error in output layer
		Double errorSum = 0d;
		for (int nIdx = 0; nIdx < errors[errors.length-1].length; nIdx++) {
			errorSum += errors[errors.length-1][nIdx];
		}
		return errorSum;
	}
}
