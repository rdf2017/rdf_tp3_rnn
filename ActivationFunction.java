package rdf_tp3_rnn;

/**
 * Utility class to store variables and algorithm depending on the activation function chosen during the training
 * @author maxim
 *
 */
public enum ActivationFunction {
	SIGN(
		new Runnable<Double>() {
			@Override
			public Double run(Double arg) {
				if (arg >= 0d) return 1d;
				else return -1d;
			}},
		new Runnable<Double>() {
				@Override
				public Double run(Double arg) {
					throw new UnsupportedOperationException("The sign function does not have a derivation");
				}},
		-1d, 1d
	),
	TANH(
		new Runnable<Double>() {
			@Override
			public Double run(Double arg) {
				final double ex = Math.exp(arg);
				final double emoinsx = Math.exp(-arg);
				return (ex - emoinsx) / (ex + emoinsx);
			}},
		new Runnable<Double>() {
			@Override
			public Double run(Double arg) {
				return 1d - arg * arg;
			}},
		-1d, 1d
	),
	SIGMOID(
		new Runnable<Double>() {
			@Override
			public Double run(Double arg) {
				return 1d / (1d + Math.exp(-arg));
			}},
		new Runnable<Double>() {
			@Override
			public Double run(Double arg) {
				return arg - arg * arg;
			}},
		0d, 1d
	);
	
	private final Runnable<Double> function, derived;
	private final double minValue, maxValue;
	private static final double ERROR_MARGIN = 0;//1e-2;
	
	ActivationFunction(Runnable<Double> function, Runnable<Double> derived, double minValue, double maxValue) {
		this.function = function;
		this.derived = derived;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	public static ActivationFunction get(int i) {
		return values()[i];
	}
	
	public Runnable<Double> getFunction() {
		return function;
	}
	
	public Runnable<Double> getDerived() {
		return derived;
	}

	public double getMinValue() {
		return minValue;
	}

	public double getMaxValue() {
		return maxValue;
	}

	public double getErrorTreshold() {
		return Double.max(Math.abs((maxValue - minValue)) / 2d - ERROR_MARGIN, 0d);
	}
}
