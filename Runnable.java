package rdf_tp3_rnn;

/**
 * 
 * @author maxim
 *
 * @param <T>
 */
public interface Runnable<T> {
	/**
	 * 
	 * @param arg
	 * @return
	 */
	T run(T arg);
}
