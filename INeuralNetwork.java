package rdf_tp3_rnn;

public interface INeuralNetwork {
	/**
	 * Training method for a network
	 * @param sample
	 * @param desired
	 * @return the error measured
	 */
	Double train(Double[] sample, Double desired);

	/**
	 * @return this output layer
	 */
	public Double[][][] getAllLayerWeights();
	
	/**
	 * Compute a value representing the class of a given sample decided by this neural network
	 * @param sample
	 * @return the decided class for the given sample
	 */
	public Double decide(Double[] sample);
}
