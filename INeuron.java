package rdf_tp3_rnn;

/**
 * 
 * @author maxim
 *
 */
public interface INeuron {
	/**
	 * Compute the class of a given sample
	 * @param sample
	 * @return the decided class
	 */
	Double decide(Double[] sample);
	
	/**
	 * Compute the correction to apply to this neurons weights
	 * @param error the error between the desired class and the class resulting of this decide method call
	 * @param decided the class resulting from this decide method call
	 * @param desired the desired class for the given sample
	 * @return the weights correction (delta)
	 */
	Double[] computeWeightsDelta(Double[] sample, Double decided, Double desired);
	
	/**
	 * Set this neuron's weights as an addition between its current weights and the given delta 
	 * @param delta weights correction
	 * @return the new weights
	 */
	Double[] applyWeightsDelta(Double[] delta);
	
	/**
	 * @return this neuron's weights
	 */
	Double[] getWeights();
	
	Double sigma(Double[] sample);
	
	Double sigmap(Double[] sample);
}
