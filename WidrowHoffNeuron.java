package rdf_tp3_rnn;

/**
 * @author maxim
 */
public class WidrowHoffNeuron extends ANeuron {

	public WidrowHoffNeuron(Double[] weights) {
		super(weights);
	}

	@Override
	public Double decide(Double[] sample) {
		return sigma(sample);
	}

	@Override
	public Double[] computeWeightsDelta(Double[] sample, Double error, Double desired) {
		final Double resp = sigmap(sample);
		final Double[] delta = new Double[getWeights().length];
		delta[0] = - getLearnRate() * (- error * resp);
		for (int i = 0; i < sample.length; i++) {
			delta[i+1] = - getLearnRate() * (- error * resp * sample[i]);
		}
		//System.out.println("sample=" + sample.toString() + " res=" + res + " wanted=" + classValue + " err=" + error + " weights=[" + (int)(weights[1]*100) + ", " + (int)(weights[2]*100) + ", " + (int)(weights[0]*100) + "]");
		return delta;
	}
}
